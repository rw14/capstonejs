export function renderDSSP (dssp){
    var contentHTML = "";
  
    dssp.forEach((sp) => {
      var contentTr = `
    <tr>
      <td style="width:5%">${sp.id}</td>
      <td style="width:10%">${sp.name}</td>
      <td style="width:10%">${new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(sp.gia)} VNĐ</td>
      <td style="width:40%" class="py-4" id="AnhSP">
        <img src=${sp.hinhanh} alt="" />
      </td>
      <td style="width:20%">${sp.mota}</td>
      <td style="width:15%">
        <button 
          onclick = xoaSanPham('${sp.id}')
          class="btn btn-danger">Xoá</button>
        <button
          onclick = suaSanPham('${sp.id}')
          data-toggle="modal"
          data-target="#myModal2"
        class="btn btn-info">Xem</button>
      </td>
    </tr>`;
      contentHTML += contentTr;
    });
    // console.log("contentHTML: ", contentHTML);
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
  };