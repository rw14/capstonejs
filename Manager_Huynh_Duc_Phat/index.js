import { renderDSSP } from "./controller/sp.controller.js";
import { validate } from "./model/validator.js";

const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const tabs = $$(".tab-item");
const panes = $$(".tab-pane");

const tabActive = $(".tab-item.active");
const line = $(".tabs .line");

requestIdleCallback(function () {
  line.style.left = tabActive.offsetLeft + "px";
  line.style.width = tabActive.offsetWidth + "px";
});

tabs.forEach((tab, index) => {
  const pane = panes[index];

  tab.onclick = function () {
    $(".tab-item.active").classList.remove("active");
    $(".tab-pane.active").classList.remove("active");

    line.style.left = this.offsetLeft + "px";
    line.style.width = this.offsetWidth + "px";

    this.classList.add("active");
    pane.classList.add("active");
  };
});
// Start
const BASE_URL = "https://62db6ce8e56f6d82a7728883.mockapi.io";

function getDSSP() {
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      renderDSSP(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

getDSSP();

function ThemSP(){
  let TenSP = document.getElementById("name").value;
  let GiaSP = document.getElementById("giaSP").value;
  let HinhAnhSP = document.getElementById("HinhAnhSP").value;
  let MotaSP = document.getElementById("NoteSP").value;
  
  let isValid = validate.kiemTraRong(TenSP, "tbTen", "Tên sản phẩm không được để trống!!!");

  isValid = isValid &
  validate.kiemTraRong(GiaSP, "tbGia", "Giá sản phẩm không được để trống!!!") &&
  validate.kiemtraSo(GiaSP, "tbGia", "Giá sản phẩm phải nhập số, không nhập khoảng cách hay chấm phẩy");

  isValid = isValid &
  validate.kiemTraRong(HinhAnhSP, "tbAnhSP", "Hình ảnh sản phẩm không được để trống!!!");

  isValid = isValid &
  validate.kiemTraRong(MotaSP, "tbNoteSP", "Mô tả về sản phẩm không được để trống!!!");

  if(isValid){

    let newSP = {
      id: "9",
      name: TenSP,
      gia: GiaSP, 
      hinhanh: HinhAnhSP,
      mota: MotaSP,
    };
    axios({
      url: `${BASE_URL}/products`,
      method: "POST",
      data: newSP,
    })
      .then(function (res) {
        console.log(res);
          document.getElementById("name").value = "";
          document.getElementById("giaSP").value = "";
          document.getElementById("HinhAnhSP").value = "";
          document.getElementById("NoteSP").value = "";
        getDSSP();
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}

function xoaSanPham(id){
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSP();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function suaSanPham(id) {
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      const datasp = res.data;
        document.getElementById("STT").value = id;
        document.getElementById("name2").value = datasp.name;
        document.getElementById("giaSP2").value = datasp.gia;
        document.getElementById("HinhAnhSP2").value = datasp.hinhanh;
        document.getElementById("NoteSP2").value = datasp.mota;
    })
    .catch(function (err) {
      console.log(err);
    });
}

function CapNhatSP(){
  let NewName = document.getElementById("name2").value;
  let NewPrice = document.getElementById("giaSP2").value;
  let NewImage = document.getElementById("HinhAnhSP2").value;
  let NewNote = document.getElementById("NoteSP2").value;
  let idNew = document.getElementById("STT").value;

  let isValid = validate.kiemTraRong(NewName, "tbTen2", "Tên sản phẩm không được để trống!!!");

  isValid = isValid &
  validate.kiemTraRong(NewPrice, "tbGia2", "Giá sản phẩm không được để trống!!!") &&
  validate.kiemtraSo(NewPrice, "tbGia2", "Giá sản phẩm phải nhập số, không nhập khoảng cách hay chấm phẩy");

  isValid = isValid &
  validate.kiemTraRong(NewImage, "tbAnhSP2", "Hình ảnh sản phẩm không được để trống!!!");

  isValid = isValid &
  validate.kiemTraRong(NewNote, "tbNoteSP2", "Mô tả về sản phẩm không được để trống!!!");

  if(isValid){

    let CapNhatSP = {
      id: idNew,
      name: NewName,
      gia: NewPrice, 
      hinhanh: NewImage,
      mota: NewNote,
    }
    axios({
      url: `${BASE_URL}/products/${idNew}`,
      method: "PUT",
      data: CapNhatSP,
    })
      .then(function (res) {
        console.log(res);
        document.getElementById("name2").value = "";
        document.getElementById("giaSP2").value = "";
        document.getElementById("HinhAnhSP2").value = "";
        document.getElementById("NoteSP2").value = "";
        document.getElementById("STT").value = "";
        getDSSP();
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}

function TimKiemSP() {
  let timKiemSP = document.getElementById("searchName").value;
  let kq = [];
  var dem = 0;
  if(timKiemSP == ""){
    document.getElementById("thongbaoTim").style.display = "block";
    document.getElementById("thongbaoTim").innerHTML = `Chưa nhập thông tin cần tìm`;
  }else{
    document.getElementById("thongbaoTim").style.display = "none";
    axios({
      url: `${BASE_URL}/products`,
      method: "GET",
    })
      .then(function (res) {
        console.log(res);
        let DSSP = res.data;
        for(let i=0;i<DSSP.length;i++){
          var sps = DSSP[i].name;
          var sp = DSSP[i];
          if(sps.includes(timKiemSP)){
            console.log("danh sách", sp);
            kq.push(sp);
            renderDSSP(kq);
            console.log("Ok");
          }else{
            dem++;
            console.log("hỏng rồi!!!");
          }
          if(dem == DSSP.length){
            console.log("không tìm thấy sp");
            document.getElementById("thongbaoTim").style.display = "block";
            document.getElementById("thongbaoTim").innerHTML = `không tìm thấy sản phẩm`;
          }
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}

window.TimKiemSP = TimKiemSP;
window.CapNhatSP = CapNhatSP;
window.suaSanPham = suaSanPham;
window.xoaSanPham = xoaSanPham;
window.ThemSP = ThemSP;