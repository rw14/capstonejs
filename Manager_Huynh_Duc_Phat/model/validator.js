export let validate = {
    kiemTraRong: function(value, idError, message){
        if(value.length == 0){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    kiemtraSo: function(value, idError, message){
        var regex=/^[0-9]+$/;        
        if(regex.test(value))
        {
            document.getElementById(idError).innerText = "";
            return true;
        }
        else
        {
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
};

