import { ITEMCARTLIST_LOCALSTORAGE } from "./index.js";

// render
export function renderData(data) {
  var contentHTML = "";
  data.forEach((item, index) => {
    let { name, price, screen, backCamera, frontCamera, img, desc, type } =
      item;
    var contentCard = /*html*/ ` 
     
    <div class="card">
    <div class="card__top"><p class="type-phone">${type}</p><em>In stock</em></div>
    <div class="card__img">
      <img src="${img}" />
    </div>
    <div class="card__detail">
      <div class="detail__name">
        <strong>${name}</strong>
        <button class="heart"><i class="fa fa-heart"></i></button>
      </div>
      <div class="detail__descrip">
        <h4>${desc}</h4>
        <ul>
          <li>
            <p>Màn hình</p>
            <div>${screen}</div>
          </li>
          <hr />
          <li>
            <p>Camera sau</p>
            <div>${backCamera}</div>
          </li>
          <hr />
          <li>
            <p>Camera trước</p>
            <div>${frontCamera}</div>
          </li>
        </ul>
      </div>
      <div class="detail__purchase">
        <span class="price">$ ${price}</span>
        <span class="btn-add" onclick="addCart('${index}')">Add to cart <i class="fa fa-plus"></i></span>
      </div>
    </div>
  </div>
      `;
    contentHTML += contentCard;
  });
  document.getElementById("body_banner").innerHTML = contentHTML;
}

// render cart

export function renderCart(data) {
  if (data.length == 0) {
    document.getElementById(
      "cart__body"
    ).innerHTML = /*html*/ `<p id="cart__empty">Looks Like You Haven't Added Any Product In The Cart</p>`;
  } else {
    // document.getElementById("cart__empty").style.display = "none";
    let contentHTML = "";

    data.map((item, index) => {
      let { name, price, screen, backCamera, frontCamera, img, desc, type } =
        item.product;
      let contentItem = /*html*/ `
  <div class="cart__item">
    <div class="item__img">
      <img
        src="${img}"
      />
    </div>
    <strong>${name}</strong>
    <div class="item__amount">
      <button onclick="quantityDecrease('${index}')"><i class="fa fa-minus decrease"></i></button>
      <span>${item.quantity}</span>
      <button onclick="quantityIncrease('${index}')"><i class="fa fa-plus increase"></i></button>
    </div>
    <div class="item__price">$ ${price * item.quantity}</div>
    <button class="item__remove" onclick="deleteItemCart('${index}')">
      <i class="fa fa-trash"></i>
    </button>
  </div>
      `;
      contentHTML += contentItem;
    });

    document.getElementById("cart__body").innerHTML = contentHTML;
  }
  localStorage.setItem(ITEMCARTLIST_LOCALSTORAGE, JSON.stringify(data));

  // total cart
  let total__products = 0;
  data.map((item) => (total__products += item.quantity));
  document.getElementById("total__products").innerHTML = total__products;

  let total__money = 0;
  data.map((item) => (total__money += item.quantity * item.product.price));
  document.getElementById("total__money").innerHTML =
    total__money.toLocaleString();
}
// notification

// document.getElementById("thank").innerHTML = "Thanks for buying";
