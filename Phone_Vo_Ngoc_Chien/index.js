const BASE_URL = "https://62e6425969bd03090f6dd80a.mockapi.io";

export const PRODUCTSLIST_LOCALSTORAGE = "PRODUCTSLIST";
export const ITEMCARTLIST_LOCALSTORAGE = "ITEMCARTLIST";

import { renderCart, renderData } from "./controller.js";

import { batLoading, closeNav, openNav, tatLoading } from "./utils.js";

let productsList = [];
let itemCartList = [];

function layData() {
  batLoading();
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then(function (res) {
      // renderData(res.data);

      localStorage.setItem(PRODUCTSLIST_LOCALSTORAGE, JSON.stringify(res.data));

      productsList = JSON.parse(
        localStorage.getItem(PRODUCTSLIST_LOCALSTORAGE)
      );

      renderData(productsList);

      let itemCartListJson = localStorage.getItem(ITEMCARTLIST_LOCALSTORAGE);
      if (itemCartListJson != null) {
        // console.log("get");
        itemCartList = JSON.parse(itemCartListJson);
        renderCart(itemCartList);
      } else {
        // console.log("no get");
        // renderCart(itemCartList);
      }
      tatLoading();
    })
    .catch(function (err) {
      // console.log("err: ", err);
      tatLoading();
    });
}
layData();

let itemCartListJson = localStorage.getItem(ITEMCARTLIST_LOCALSTORAGE);
if (itemCartListJson != null) {
  // console.log("get");
  itemCartList = JSON.parse(itemCartListJson);
  renderCart(itemCartList);
} else {
  // console.log("no get");
}

window.itemCartList = itemCartList;

// open cart
document.getElementById("btn__cart").addEventListener("click", openNav);
// close cart
document.getElementById("cart__bg").addEventListener("click", closeNav);
document.getElementById("closeNav").addEventListener("click", closeNav);

// filter
document.getElementById("type").onchange = function () {
  // console.log("productsList: ", productsList);
  let type = document.getElementById("type").value;
  if (type != 0) {
    renderData(productsList.filter((item) => item.type === type));
  } else {
    renderData(productsList);
  }
};
window.productsList = productsList;

const clear = document.querySelectorAll(".clear");
for (let i = 0; i < clear.length; i++) {
  clear[i].addEventListener("click", function () {
    itemCartList = [];
    renderCart(itemCartList);
  });
}
