import { renderCart } from "./controller.js";
import { PRODUCTSLIST_LOCALSTORAGE } from "./index.js";
import { ItemCart } from "./model.js";

// loading
export function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
export function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

// Sidenav
export function openNav() {
  document.getElementById("cart").style.width = "100vw";
  document.getElementById("cart__content").style.display = "flex";
}
export function closeNav() {
  document.getElementById("cart").style.width = "0vw";
  document.getElementById("cart__content").style.display = "none";
}
// delete item from cart

function deleteItemCart(index) {
  itemCartList.splice(index, 1);
  renderCart(itemCartList);
}
window.deleteItemCart = deleteItemCart;
// add to cart

function isTrungSP(list, name) {
  const index = list.findIndex((item) => item.product.name === name);
  return index;
}
export function addCart(index) {
  productsList = JSON.parse(localStorage.getItem(PRODUCTSLIST_LOCALSTORAGE));

  if (isTrungSP(itemCartList, productsList[index].name) != -1) {
    itemCartList[isTrungSP(itemCartList, productsList[index].name)].quantity++;
  } else {
    let itemCart = new ItemCart(productsList[index], 1);

    itemCartList.push(itemCart);
  }

  renderCart(itemCartList);
}
window.addCart = addCart;

// change quantity item
// -
function quantityDecrease(index) {
  if (itemCartList[index].quantity > 0) {
    itemCartList[index].quantity--;
  } else {
    deleteItemCart(index);
  }
  renderCart(itemCartList);
}
window.quantityDecrease = quantityDecrease;
// +
function quantityIncrease(index) {
  itemCartList[index].quantity++;
  renderCart(itemCartList);
}
window.quantityIncrease = quantityIncrease;
